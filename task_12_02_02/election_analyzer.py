# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_12_02_02.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import csv
import matplotlib.pyplot as plt


class ElectionAnalyzer:
    """Класс ElectionAnalyzer выполняет:
      - формирование итогового списка партий с подсчетом голосов;
      - построение круговой диаграммы суммарного количества голосов.

    Поля:
      - self._data: двумерный массив данных вида:
             [[1, 'Партия 5', 1, 1, 2, 0.16666666666666666],
              [2, 'Партия 4', 2, 2, 4, 0.3333333333333333],
              [3, 'Партия 3', 3, 3, 6, 0.5]],

        где каждая строка (внутренний список) содержит:
        - номер партии в бюллетене;
        - наименование партии;
        - кол-во голосов за партию на 1-м участке;
        - кол-во голосов за партию на 2-м участке;
        - ...;
        - кол-во голосов за партию на N-м участке;
        - сумма голосов за партию;
        - доля голосов за партию от общего кол-ва голосов.

    Методы:
      - self.load(): загрузка, определение показателей и сортировка данных;
      - self._make_plot(): формирование изображения;
      - self.show_plot(): отображение изображения.
    """

    def __init__(self):
        """Инициализация класса.

        Действия:
          - инициализировать 'self._data'.
        """
        raise NotImplementedError
        # Уберите raise и дополните код

    def __str__(self):
        """Вернуть строку - анализируемые данные.

        Формат:

        Результаты (14):
        1. ЕДИНАЯ РОССИЯ 55.23%
        ...
        14. Гражданская Сила 0.14%

        Первое число - номер партии в списке 'self._data'.
        """
        raise NotImplementedError
        # Уберите raise и дополните код

    def load(self, filename):
        """Загрузить файл 'filename' с результатами выборов.

        Параметры:
          - filename (str): имя файла.

        По результатам загрузки 'self._data' должен содержать
        двумерный набор данных и файла, включая заголовки.
        Числовые значения должны быть преобразованы в числа.

        Функция не обрабатывает возникающие исключения.
        """
        raise NotImplementedError
        # Уберите raise и дополните код

    def _make_plot(self):
        """Генерирует изображение, не отображая его.

        Изображение должно включать несколько диаграмм по горизонтали,
        количество которых равно длине 'self._data'.

        Если 'self._data' не содержит данных, возбудить AssertionError.

        Длина оси OX для каждой диаграммы должна быть вдвое больше
        значения точки безубыточности.

        Результат:
          - fig: matplotlib.figure.Figure.
        """
        raise NotImplementedError
        # Уберите raise и дополните код

        # Смещение оси и легенды
        ax.set_position([0.2, 0.1, 1.5, 0.8])
        plt.legend(labels, bbox_to_anchor=(1., 0.8))
        ax.set_aspect("equal")

        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.5, top=0.85,
                            wspace=0.2, hspace=0.2)
        fig.tight_layout()

        return fig

    def show_plot(self):
        """Создать изображение и показать его на экране."""
        self._make_plot()
        plt.show()
